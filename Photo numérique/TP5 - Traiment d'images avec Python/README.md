# 2nde SNT

## PHOTO NUMERIQUE / TP5 - Traiment d'images avec Python


Pour faire le TP :

- Cliquez sur ce bouton : [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/plore%2Fsnt/HEAD)
- et ouvrez le fichier TP5 - Traitement d'image avec Python.ipynb dans les dossiers "Photo numérique/TP5 - Traiment d'images avec Python/"

![TP5 - Traitement d'image avec Python.ipynb](images/doc/mode_emploi.jpg)


## M'envoyez votre travail

- Vérifiez que vous avez correctement nommé votre fichier : **NumClasse-1èreLETTREdevotreNOMPrenom-Photo-TP5.ipynb** (par exemple 2nde1-DPierre-PhotoTP5.ipynb)
- Si c'est le cas, **envoyez-moi votre fichier notebook (ipynb)** comme compte-rendu de travail **via ce [formulaire](https://script.google.com/macros/s/AKfycbwrgjmlVHourETcan8d9TC8PO61zqwNxwowYeVQDyAcjd6qhXEU/exec)**

[![Formulaire de dépôt de TP](../../img_doc/formulaire_envoi.jpg)](https://script.google.com/macros/s/AKfycbwrgjmlVHourETcan8d9TC8PO61zqwNxwowYeVQDyAcjd6qhXEU/exec)

<iframe src='https://script.google.com/macros/s/AKfycbwrgjmlVHourETcan8d9TC8PO61zqwNxwowYeVQDyAcjd6qhXEU/exec'></iframe>



---
[Pour plus de détails sur l'utilisation des notebooks](https://gitlab.com/plore/snt/-/blob/master/README.md)