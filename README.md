# 2nde SNT
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/plore%2Fsnt/HEAD)


## Présentation des TPs en version Jupyter Notebook

Un Jupyter Notebook est un **document qui mélange du texte et du code Python**.

Il est modifiable et peut être enregistré sous différents formats (ipynb, format original ; pdf, html, etc...).



## Utilisation d'un Notebook

Chaque partie de texte et de code est insérée dans des **cellules**.
Dans une **cellule de code**, pour afficher la sortie d'un programme Python, il faut **l'exécuter** ou **valider la cellule**. Pour cela :
- on se place dans la cellule
- on appuie ensuite sur '**MAJ**' + '**ENTER**'  ou sur le bouton '**RUN**'

[![Présentation notebook](img_doc/present_jupyter.jpg)](https://mybinder.org/v2/gl/plore%2Fsnt/HEAD)



## Pour faire un TP

- Cliquez sur ce bouton : [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/plore%2Fsnt/HEAD)
- et ouvrez le TP du jour **en parcourant l'arborescence proposée**

[![Recherche TP](img_doc/mode_emploi.jpg)](https://mybinder.org/v2/gl/plore%2Fsnt/HEAD)



## Enregistrer votre travail

Lorsque votre travail sera fini, il vous faudra : 
- **enregistrer** votre Notebook (cf. capture ci-dessus bouton "download")
- le nommer **NumClasse-1èreLETTREdevotreNOMPrenom-Thème-TPnuméro.ipynb** (par exemple 21-DPierre-PhotoTP5.ipynb)
- et **me l'envoyer en tant que fichier** comme compte-rendu de travail **via ce [formulaire](https://script.google.com/macros/s/AKfycbwrgjmlVHourETcan8d9TC8PO61zqwNxwowYeVQDyAcjd6qhXEU/exec)** (cf. détails ci-dessous)

**ATTENTION :** si vous quittez le Notebook, il sera automatiquement remis dans sa forme initiale **et vos modifications seront perdues**.



## M'envoyez votre travail

- Vérifiez que vous avez correctement nommé votre fichier : **NumClasse-1èreLETTREdevotreNOMPrenom-Thème-TPnuméro.ipynb** (par exemple 21-DPierre-PhotoTP5.ipynb)
- Si c'est le cas, **envoyez-moi votre fichier notebook (ipynb)** comme compte-rendu de travail **via ce [formulaire](https://script.google.com/macros/s/AKfycbwrgjmlVHourETcan8d9TC8PO61zqwNxwowYeVQDyAcjd6qhXEU/exec)**

[![Formulaire de dépôt de TP](img_doc/formulaire_envoi.jpg)](https://script.google.com/macros/s/AKfycbwrgjmlVHourETcan8d9TC8PO61zqwNxwowYeVQDyAcjd6qhXEU/exec)

<iframe src='https://script.google.com/macros/s/AKfycbwrgjmlVHourETcan8d9TC8PO61zqwNxwowYeVQDyAcjd6qhXEU/exec'></iframe>



---
[Source](https://framagit.org/coubertin_nsi/snt_courses/-/blob/master/README.md)